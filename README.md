# xcx

## 第一步下载依赖
```
npm install
```

### 启动项目
```
npm run serve
```

### 打包部署
```
npm run build
```

### 项目介绍

项目名称：拖拽排序生成小程序
左侧：为组件库
中间：展示区域，可拖拽实现组件的排序
右侧：为修改组件的样式属性
项目介绍
本项目由vue+vuedraggable+axios+elementui组件库实现
#### 如遇node版本过高问题
window：NODE_OPTIONS="--openssl-legacy-provider"

linux：NODE_OPTIONS=--openssl-legacy-provider 
#### 如果对你有帮助请给⭐⭐⭐⭐⭐ star
##### 如遇不懂可评论讨论

